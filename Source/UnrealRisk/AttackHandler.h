// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Space.h"
#include "Structs.h"
#include "AttackHandler.generated.h"

UCLASS()
class UNREALRISK_API AAttackHandler : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAttackHandler();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		ASpace* attacker;

	UPROPERTY(EditAnywhere)
		ASpace* defender;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		FBattleReport Battle(int aRoll, int dRoll);

	UFUNCTION(BlueprintCallable)
		void setSelection1(ASpace* space);

	UFUNCTION(BlueprintCallable)
		void setSelection2(ASpace* space);

	UFUNCTION(BlueprintCallable)
		ASpace* GetSelection1();

	UFUNCTION(BlueprintCallable)
		ASpace* GetSelection2();

	UFUNCTION(BlueprintCallable)
		bool ifFromSelected();

	UFUNCTION(BlueprintCallable)
		void ClearSelections();
};

