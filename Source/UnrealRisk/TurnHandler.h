// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enums.h"
#include "TurnHandler.generated.h"

UCLASS()
class UNREALRISK_API ATurnHandler : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATurnHandler();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		int firstPlayer;
	
	UPROPERTY(EditAnywhere)
		int currentPlayer;

	UPROPERTY(EditAnywhere)
		int currentReinfTroops;

	UPROPERTY(EditAnywhere)
		EPhases currentPhase;

	UPROPERTY(EditAnywhere)
		TArray<FString> names;

	UPROPERTY(EditAnywhere)
		TArray<EPlayers> players;

	UPROPERTY(EditAnywhere)
		TArray<EColors> colors;

	UPROPERTY(EditAnywhere)
		EDifficulties difficulties[6];

	UPROPERTY(EditAnywhere)
		int reinforcements[6];

	UFUNCTION(BlueprintCallable)
	void SetMembers(TArray<FString> nameArray, TArray<FString> colorArray, TArray<FString> diffArray);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		int GetFirstPlayer();

	UFUNCTION(BlueprintCallable)
		EPlayers GetCurrentPlayer();

	UFUNCTION(BlueprintCallable)
		EColors GetCurrentPlayerColor();

	UFUNCTION(BlueprintCallable)
		EPhases GetCurrentPhase();

	UFUNCTION(BlueprintCallable)
		int GetCurrentReinfTroops();

	UFUNCTION(BlueprintCallable)
		EDifficulties GetDifficulty(int player);

	UFUNCTION(BlueprintCallable)
		int GetReinforcments(int player);

	UFUNCTION(BlueprintCallable)
		TArray<EPlayers> GetPlayers();

	UFUNCTION(BlueprintCallable)
		TArray<FString> GetNames();

	UFUNCTION(BlueprintCallable)
		TArray<EColors> GetColors();

	UFUNCTION(BlueprintCallable)
		void IncrementTurn();

	UFUNCTION(BlueprintCallable)
		void IncrementPhase();

	UFUNCTION(BlueprintCallable)
		void Knockout(EPlayers player);

	UFUNCTION(BlueprintCallable)
		void SetCurrentPlayer(int first);

	UFUNCTION(BlueprintCallable)
		void SetCurrentReinfTroops(int value);

	UFUNCTION(BlueprintCallable)
		void SetReinforcments(int player, int value);

	UFUNCTION(BlueprintCallable)
		void DecCurrentReinfTroops();

};
