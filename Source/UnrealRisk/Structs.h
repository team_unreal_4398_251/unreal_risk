#pragma once
#pragma once

#include "CoreMinimal.h"
#include "Enums.h"
#include "Space.h"
#include "Structs.generated.h"

USTRUCT(Blueprintable)
struct FSpaceLocation
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EConinents cont;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int space;
};

USTRUCT(Blueprintable)
struct FSpacePair
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ASpace* SelectionOne;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ASpace* SelectionTwo;
};

USTRUCT(Blueprintable)
struct FBattleReport
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ACasualties;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int DCasualties;
};

