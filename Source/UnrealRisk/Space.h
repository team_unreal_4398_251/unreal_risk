// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enums.h"
#include "Structs.h"
#include "Space.generated.h"

UCLASS()
class UNREALRISK_API ASpace : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpace();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//UPROPERTY(EditAnywhere)
		//EConinents boardLocation;

	//UPROPERTY(EditAnywhere)
		//int contLocation;

	UPROPERTY(EditAnywhere)
		EPlayers ownr;
	
	UPROPERTY(EditAnywhere)
		int troops;

	UPROPERTY(EditAnywhere)
		bool eventBool;

	UPROPERTY(EditAnywhere)
		bool pauseBool;

	UPROPERTY(EditAnywhere)
		AActor* marker;

	UPROPERTY(EditAnywhere)
		TArray<ASpace*> connections;

	UPROPERTY(EditAnywhere)
		bool visited;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Claim Space
	UFUNCTION(BlueprintCallable)
		void ClaimSpace(EPlayers newOwner);

	// get Space owner
	UFUNCTION(BlueprintCallable)
		EPlayers GetOwnr();

	UFUNCTION(BlueprintCallable)
		int GetTroops();

	UFUNCTION(BlueprintCallable)
		AActor* GetMarker();

	UFUNCTION(BlueprintCallable)
		TArray<ASpace*> GetConnections();

	UFUNCTION(BlueprintCallable)
		bool GetVisited();

	UFUNCTION(BlueprintCallable)
		void EventOff();

	UFUNCTION(BlueprintCallable)
		void EventOn();

	UFUNCTION(BlueprintCallable)
		bool GetEventBool();

	UFUNCTION(BlueprintCallable)
		void PauseOff();

	UFUNCTION(BlueprintCallable)
		void PauseOn();

	UFUNCTION(BlueprintCallable)
		bool GetPauseBool();

	UFUNCTION(BlueprintCallable)
		void IncrementTroops();

	UFUNCTION(BlueprintCallable)
		void DecrementTroops();

	UFUNCTION(BlueprintCallable)
		bool IsBorder(EPlayers player);

	UFUNCTION(BlueprintCallable)
		void TurnOnGlobalConnectionEvents();

	UFUNCTION(BlueprintCallable)
		void TurnOnBattleEvents();

	UFUNCTION(BlueprintCallable)
		void EnableVisited();

	UFUNCTION(BlueprintCallable)
		void DisableVisited();


};
