// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Space.h"
#include "Enums.h"
#include "Structs.h"
#include <limits>
#include "Board.generated.h"

UCLASS()
class UNREALRISK_API ABoard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TArray<ASpace*> naSpaces;

	UPROPERTY(EditAnywhere)
		TArray<ASpace*> saSpaces;

	UPROPERTY(EditAnywhere)
		TArray<ASpace*> afSpaces;

	UPROPERTY(EditAnywhere)
		TArray<ASpace*> euSpaces;

	UPROPERTY(EditAnywhere)
		TArray<ASpace*> asSpaces;

	UPROPERTY(EditAnywhere)
		TArray<ASpace*> auSpaces;

	UFUNCTION(BlueprintCallable)
		void GBCSHelper(TArray<ASpace*>& arry, ASpace* space);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void TurnOffBoardEvents();

	UFUNCTION(BlueprintCallable)
		void TurnOnUnclaimedEvents();

	UFUNCTION(BlueprintCallable)
		bool CheckForKnockout(EPlayers losingPlayer);

	UFUNCTION(BlueprintCallable)
		bool CheckForUnclaimed();

	UFUNCTION(BlueprintCallable)
		bool CheckForWinner();

	UFUNCTION(BlueprintCallable)
		FSpaceLocation EasyBeginAiSelectSpace();

	UFUNCTION(BlueprintCallable)
		FSpaceLocation EasyReinfAiSelectSpace(EPlayers currentPlayer);

	UFUNCTION(BlueprintCallable)
		ASpace* EasyAttackAiSelectAttackSpace(EPlayers currentPlayer);

	UFUNCTION(BlueprintCallable)
		ASpace* EasyAttackAiSelectDeffendSpace(ASpace* attackSpace);

	UFUNCTION(BlueprintCallable)
		int GetLowestBorderValue(EPlayers currentPlayer);

	UFUNCTION(BlueprintCallable)
		ASpace* GetSpace(EConinents cont, int space);

	UFUNCTION(BlueprintCallable)
		FSpacePair GetTransferPair(EPlayers currentPlayer);

	UFUNCTION(BlueprintCallable)
		ASpace* HardAttackAiSelectAttackSpace(EPlayers currentPlayer);

	UFUNCTION(BlueprintCallable)
		ASpace* HardAttackAiSelectDeffendSpace(ASpace* attackSpace);

	UFUNCTION(BlueprintCallable)
		bool SearchForNonBorderExtraTroops(EPlayers currentPlayer);

	UFUNCTION(BlueprintCallable)
		void TurnOnPlayerSpaceEvents();

	UFUNCTION(BlueprintCallable)
		void TurnOnPlayerTransferEvents();

	UFUNCTION(BlueprintCallable)
		void TurnOnBorderEvents();

	UFUNCTION(BlueprintCallable)
		int CalculateReinforcements(int player);

	UFUNCTION(BlueprintCallable)
		TArray<ASpace*> GetBordersConnectedToSpace(ASpace* space);

};
